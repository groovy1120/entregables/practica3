package com.curso.heroes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.heroes.entities.Heroes;


@Repository
public interface HeroesRepository extends JpaRepository<Heroes, Integer> {}
