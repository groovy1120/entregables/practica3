package com.curso.heroes.controllers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.curso.heroes.entities.Heroes;
import com.curso.heroes.repositories.HeroesRepository;


@RestController
@RequestMapping("/heroes")
public class HeroesController {
	
	HeroesRepository heroesRepository;
	
	@PersistenceContext
    private EntityManager em;
	
    @Autowired
    public HeroesController(HeroesRepository heroesRepository) {
        this.heroesRepository = heroesRepository;
    }
    
    @CrossOrigin(origins = "*")
    @RequestMapping(headers="Accept=application/json", method = RequestMethod.GET)
    public List<Heroes> findAll(){
        return heroesRepository.findAll();
    }
    
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/{superheroName}", headers="Accept=application/json", method = RequestMethod.GET)
    public List<Heroes> findBySuperheroName(@PathVariable String superheroName){
        Query q = em.createQuery("SELECT h FROM superhero AS h WHERE h.superheroName = '" + superheroName +"'");
        //q.setParameter("superheroName",superheroName);
        List<Heroes>result = q.getResultList();
        return result;
    }
}
